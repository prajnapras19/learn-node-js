We can use `express` to do a routing.

To use it, we need to save it into `package.json` (note to myself: remember `requirements.txt` when you do a python project? Apparently, they are the same thing).

Create it with:
```
npm init
```

How to install:
```
npm install express --save
```

The `node_modules/` folder may not be pushed because we have the `package.json`. To reinstall it, run:
```
npm install
```

### But, we used `http` in `project-000`...
After I read [this](https://www.geeksforgeeks.org/what-are-the-differences-between-http-module-and-express-js-module/), I can say that `express` is "a next level" of `http` as a framework.

References:
- https://mfikri.com/artikel/tutorial-nodejs